# Exercise 2: Write another program that prompts for a list of numbers as above and at the end prints out both the maximum and minimum of the numbers instead of the average.

def calculate(numbers, count) : 

  total = 0
  minNum = None
  maxNum = None

  for number in numbers : 

    # total calculation
    total = total + number

    # minNum calculation
    if minNum is None or number < minNum :
      minNum = number 
    
    # maxNum calculation
    if maxNum is None or number > maxNum :
      maxNum = number 

  average = total/count
  print("Count: ", count)
  print("Total: ", total)
  print("Average", average)
  print("Minimum Number", minNum)
  print("Maximum Number", maxNum)
  print("All Numbers", numbers)

numbers = []
count = 0
userInput = "notDone"

while userInput != "done" : 
  
  userInput = input("What number? ")

  if userInput == "done" :
    break

  try : 
    userInputIntTest = int(userInput)
  except : 
    print("cmon man...numbers only I said!")
    continue

  userInputInt = int(userInput)
  numbers.append(userInputInt)
  count = count + 1

calculate(numbers, count)