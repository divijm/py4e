# Exercise 1: Write a program which repeatedly reads numbers until the user enters "done". Once "done" is entered, print out the total, count, and average of the numbers. 
# If the user enters anything other than a number, detect their mistake using try and except and print an error message and skip to the next number.
# Enter a number: 4
# Enter a number: 5
# Enter a number: bad data
# Invalid input
# Enter a number: 7
# Enter a number: done
# 16 3 5.333333333333333

def calculate(numbers, count) : 
  total = 0
  for number in numbers : 
    total = total + number
  average = total/count
  print("Count: ", count)
  print("Total: ", total)
  print("Average", average)

numbers = []
count = 0
userInput = "notDone"

while userInput != "done" : 
  
  userInput = input("What number? ")

  if userInput == "done" :
    break

  try : 
    userInputIntTest = int(userInput)
  except : 
    print("cmon man...numbers only I said!")
    continue

  userInputInt = int(userInput)
  numbers.append(userInputInt)
  count = count + 1

calculate(numbers, count)