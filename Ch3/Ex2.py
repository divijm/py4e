# Exercise 2: Rewrite your pay program using try and except so that your program handles non-numeric input gracefully by printing a message and exiting the program. The following shows two executions of the program:

# Enter Hours: 20
# Enter Rate: nine
# Error, please enter numeric input
# Enter Hours: forty
# Error, please enter numeric input

hoursStr = input("How many hours did you work? ")
rateStr = input("And, what rate do you get paid at? ")

try:

  hoursInt = int(hoursStr)
  rateInt = int(rateStr)

  if hoursInt > 40 : 
    rateInt = rateInt * 1.5 

  pay = rateInt * hoursInt

  print(pay)

except: 

  print("Please enter a number next time please")
