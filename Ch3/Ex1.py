# Exercise 1: Rewrite your pay computation to give the employee 1.5 times the hourly rate for hours worked above 40 hours.
# Enter Hours: 45
# Enter Rate: 10
# Pay: 475.0

hoursStr = input("How many hours did you work? ")
rateStr = input("And, what rate do you get paid at? ")

hoursInt = int(hoursStr)
rateInt = int(rateStr)

if hoursInt > 40 : 
  rateInt = rateInt * 1.5 

pay = rateInt * hoursInt

print(pay)

