# Exercise 6: Rewrite your pay computation with time-and-a-half for overtime and create a function called computepay which takes two parameters (hours and rate).

# hoursStr = input("How many hours did you work? ")
# rateStr = input("And, what rate do you get paid at? ")
# try:
#   hoursInt = int(hoursStr)
#   rateInt = int(rateStr)
#   if hoursInt > 40 : 
#     rateInt = rateInt * 1.5 
#   pay = rateInt * hoursInt
#   print(pay)
# except: 
#   print("Please enter a number next time please")



try:

  hoursStr = input("How many hours did you work? ")
  rateStr = input("And, what rate do you get paid at? ")

  hoursInt = int(hoursStr)
  rateInt = int(rateStr)

  def computePay(hoursInt, rateInt) : 

    if hoursInt > 40 : 
      rateInt = rateInt * 1.5 

    pay = rateInt * hoursInt

    return(pay)

    # print(pay)

  pay = computePay(hoursInt, rateInt)
  print(pay)

except: 

  print("Please enter a number next time please")
