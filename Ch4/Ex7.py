# Exercise 7: Rewrite the grade program from the previous chapter using a function called computegrade that takes a score as its parameter and returns a grade as a string.
# scoreStr = input("Enter score: ")
# score = float(scoreStr)
# if 0.0 < score < 1.0 : 
#   if score >= 0.9 : 
#     print("A")
#   elif score >= 0.8 : 
#     print("B")
#   elif score >= 0.7 : 
#     print("C")
#   elif score >= 0.6 : 
#     print("D")
#   elif score < 0.6 : 
#     print("F")
# else : 
#   print("Bad score")
#   print("Enter in a score between 0 and 1 please. Also make sure you are entering in numbers and not words. I don't speak English sorry!")

def computeGrade(scoreStr) :

  score = float(scoreStr)

  if 0.0 < score < 1.0 : 
    if score >= 0.9 : 
      print("A")
    elif score >= 0.8 : 
      print("B")
    elif score >= 0.7 : 
      print("C")
    elif score >= 0.6 : 
      print("D")
    elif score < 0.6 : 
      print("F")
  else : 
    print("Bad score")
    print("Enter in a score between 0 and 1 please. Also make sure you are entering in numbers and not words. I don't speak English sorry!")

scoreStr = input("Enter score: ")

computeGrade(scoreStr)
