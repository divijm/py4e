# Exercise 1: Write a while loop that starts at the last character in the string and works its way backwards to the first character in the string, 
# printing each letter on a separate line, except backwards.

def stringWord(word) :
  print(word)
  
  length = len(word)
  
  while length > 0 :

    lastLetter = word[length-1]
    print(lastLetter)
    length = length - 1

userInput = input("What word would you like to try? ")

stringWord(userInput)